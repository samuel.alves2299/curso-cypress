//navegação em url
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));
//interação com os campos
    it("fills all the text input fields", () => {
        const firstName = "Samuel";
        const lastName = "Alves";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@gmail.com");
        cy.get("#requests").type("Carnivore");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    it("selects 'social media' checkbox", () =>{
        cy.get("#social-media").check();
    });

    it("selects 'friend', and 'publication', then uncheck 'friend'", ()=> {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    //verificações cypress
    it("has 'TICKETBOX' header's heading", () => { 
        cy.get("header h1").should("contain", "TICKETBOX");
    }); 

    //assert
    it("alerts on invalid email", () => {
        cy.get("#email")
        .as("email")
        .type("teste-gmail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("teste@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    });
    //teste e2e cypress
    it("fills and reset the form", () => {
        const firstName = "Samuel";
        const lastName = "Alves";
        const fullname = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");
    
        cy.get(".agreement p").should(
            "contain",
            `I, ${fullname}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type (fullname);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");    
        
        cy.get("button[type='reset']").click();
        
        cy.get("@submitButton").should("be.disabled");   
    });
//comando customizado
    it("fills mandatory fields using support command", () =>{
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "teste@teste.com"
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");
      
    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");


  });
});